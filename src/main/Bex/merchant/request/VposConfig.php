<?php
namespace Bex\merchant\request;

class VposConfig
{

    private $vposUserId;
    private $vposPassword;
    private $extra = array();
    private $bankIndicator;
    private $serviceUrl;

    /**
     * VposConfig constructor.
     */
    public function __construct()
    {
    }


    /**
     * @return mixed
     */
    public function getVposUserId()
    {
        return $this->vposUserId;
    }

    /**
     * @param mixed $vposUserId
     */
    public function setVposUserId($vposUserId)
    {
        $this->vposUserId = $vposUserId;
    }

    /**
     * @return mixed
     */
    public function getVposPassword()
    {
        return $this->vposPassword;
    }

    /**
     * @param mixed $vposPassword
     */
    public function setVposPassword($vposPassword)
    {
        $this->vposPassword = $vposPassword;
    }

    /**
     * @return array
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * @param array $extra
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;
    }


    /**
     * @return mixed
     */
    public function getBankIndicator()
    {
        return $this->bankIndicator;
    }

    /**
     * @param mixed $bankIndicator
     */
    public function setBankIndicator($bankIndicator)
    {
        $this->bankIndicator = $bankIndicator;
    }

    /**
     * @return mixed
     */
    public function getServiceUrl()
    {
        return $this->serviceUrl;
    }

    /**
     * @param mixed $serviceUrl
     */
    public function setServiceUrl($serviceUrl)
    {
        $this->serviceUrl = $serviceUrl;
    }


    public function addExtra($key, $value)
    {
        $this->extra[$key] = $value;
    }


}