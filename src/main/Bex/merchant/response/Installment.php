<?php
namespace Bex\merchant\response;

class Installment
{

    private $numberOfInstallment;
    private $installmentAmount;
    private $totalAmount;
    private $vposConfig;

    /**
     * Installment constructor.
     * @param $numberOfInstallment
     * @param $installmentAmount
     * @param $totalAmount
     * @param $vposConfig
     */
    public function __construct($numberOfInstallment, $installmentAmount, $totalAmount, $vposConfig)
    {
        $this->numberOfInstallment = $numberOfInstallment;
        $this->installmentAmount = $installmentAmount;
        $this->totalAmount = $totalAmount;
        $this->vposConfig = $vposConfig;
    }


    /**
     * @return mixed
     */
    public function getNumberOfInstallment()
    {
        return $this->numberOfInstallment;
    }

    /**
     * @param mixed $numberOfInstallment
     */
    public function setNumberOfInstallment($numberOfInstallment)
    {
        $this->numberOfInstallment = $numberOfInstallment;
    }

    /**
     * @return mixed
     */
    public function getInstallmentAmount()
    {
        return $this->installmentAmount;
    }

    /**
     * @param mixed $installmentAmount
     */
    public function setInstallmentAmount($installmentAmount)
    {
        $this->installmentAmount = $installmentAmount;
    }

    /**
     * @return mixed
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * @param mixed $totalAmount
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;
    }

    /**
     * @return mixed
     */
    public function getVposConfig()
    {
        return $this->vposConfig;
    }

    /**
     * @param mixed $vposConfig
     */
    public function setVposConfig($vposConfig)
    {
        $this->vposConfig = $vposConfig;
    }


}